<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/table', 'IndexController@table');

Route::get('/data-table', function(){ 
    return view('table.data-table');
});

// CRUD Cast 
// Route::get('/Cast/create', 'CastController@create'); 
// Route::post('/Cast', 'CastController@store');
// Route::get('/Cast', 'CastController@index');
// Route::get('/Cast/{Cast_id}', 'CastController@show');
// Route::get('/Cast/{Cast_id}/edit', 'CastController@edit');
// Route::put('/Cast/{Cast_id}', 'CastController@update');
// Route::delete('/Cast/{Cast_id}', 'CastController@destroy');

// CRUD Cast Resource
Route::resource('Cast', 'CastController');
// Route::post('/Castb', 'CastbController@store');
// Route::get('/Castb', 'CastbController@index');
