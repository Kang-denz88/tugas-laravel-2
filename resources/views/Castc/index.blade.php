{{-- @extends('layout.master')

@section('judul')
    Media List Cast
@endsection

@section('content')

<a href="/Cast/create" class="btn btn-success mb-3"> Tambah Cast </a>

<table class="table">
    <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col"> Nama Cast </th>
            <th scope="col"> Umur Cast </th>
            <th scope="col"> Bio Cast </th>
            <th scope="col"> Action </th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <td> {{$key + 1}} </td>
                <td> {{$item->nama}} </td>
                <td> {{$item->umur}} </td>
                <td> {{$item->bio}} </td>
                <td>
                    <form action="/Cast/{{$item->id}}" method="POST">
                    @method("delete")
                    @csrf
                        <a href="/Cast/{{$item->id}}" class="btn btn-info btn-sm"> Detail Cast </a>
                        <a href="/Cast/{{$item->id}}/edit" class="btn btn-warning btn-sm"> Edit Cast </a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td> Data Masih Kosong </td>
            </tr>
        @endforelse
    </tbody>
</table>
@endsection --}}